# -*- coding: utf-8 -*-
import sphinx_bootstrap_theme

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
]


def setup(app):
    js_params = {'data-campaign': "xqdhm185q1ibus0ve0bz",
                 'data-user': '51860',
                 'async': 'async'
                 }
    app.add_javascript('https://a.optmnstr.com/app/js/api.min.js', **js_params)
    app.add_stylesheet('./_static/my-style.css')


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.rst'


# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'Certificate Management'
copyright = u'2018, MyArch'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '3.4'
# The full version, including alpha/beta/rc tags.
release = '3.4'

# html_sidebars = {
#     '**': [
#         'about.html',
#         'navigation.html',
#         'relations.html',
#         'searchbox.html',
#     ]
# }

html_theme='bootstrap'
html_theme_path=sphinx_bootstrap_theme.get_html_theme_path()


html_logo="./_static/logo.png"

html_context = {
    'css_files': [
        '_static/theme_overrides.css',  # override wide tables in RTD theme
    ],
}

html_theme_options = {

    'navbar_title': "Demo",
    'navbar_site_name': "Site",

    'navbar_links': [
        ("Examples", "examples"),
        ("Link", "http://example.com", True),
    ],

    'navbar_sidebarrel': True,
    'navbar_pagenav': True,
    'navbar_pagenav_name': "Page",
    'globaltoc_depth': 2,
    'globaltoc_includehidden': "true",
    'navbar_class': "navbar navbar-inverse",
    'navbar_fixed_top': "true",
    'source_link_position': "nav",

    'googlewebfont': True,
    'googlewebfont_url': 'http://fonts.googleapis.com/css?family=Text+Me+One',
    'googlewebfont_style': "font-family: 'Text Me One', sans-serif",

    'bootswatch_theme': "united",
    'bootstrap_version': "3",

    'theme_preview': True,


    'logo_name': True,
    'description': 'Certificates and Security Best Practices',
    'sidebar_collapse': True,
    'fixed_sidebar': True,
    'show_powered_by': False,
    'show_related': True,
    # Enable for production:
    'analytics_id': 'UA-957603-1'
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = []


# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output ----------------------------------------------

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = "Certificate Management Best Practices"

html_static_path = ['_static']


# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
html_use_smartypants = False


# If false, no module index is generated.
html_domain_indices = False


# If true, links to the reST sources are added to the pages.
# TODO: turn off before publishing
html_show_sourcelink = False

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = False

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
html_show_copyright = False

# Output file base name for HTML help builder.
htmlhelp_basename = 'secbestpractices'


# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    ('index', 'DPBuddy.tex', u'Certificates Management',
     u'MyArch', 'manual'),
]


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'dpbuddy', u'Certificates Management',
     [u'MyArch'], 1)
]


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    ('index', 'DPBuddy', u'Certificates Management',
     u'MyArch', 'DPBuddy', 'Certificates Management',
     'Miscellaneous'),
]

# -- Options for Epub output ----------------------------------------------

# Bibliographic Dublin Core info.
epub_title = u'Certificate Management'
epub_author = u'MyArch'
epub_publisher = u'MyArch'
epub_copyright = u'2018, MyArch'
# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']
