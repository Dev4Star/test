.. _intro:

Introduction
============
Modern systems always consist of multiple components/container /services. Encrypted communication between these components is becoming a de-facto standard, pushed by the notion of a `zero-trust network <https://www.csoonline.com/article/3247848/network-security/what-is-zero-trust-a-model-for-more-effective-security.html>`_ (we don't want to assume that the internal network is always safe). Indeed, most breaches result in compromising one or multiple servers on an internal network, so in case of a plain-text communication, perpetrators would be able to freely eavesdrop on all the information on the network.

There is also an ever-growing internal threat and the principle of the least privilege. A server administrator should have an ability to read application-level data.

Container orchestrator tools, such as Kubernetes (and various service meshes, such as `Istio <https://istio.io/docs/concepts/what-is-istio/>`_), manage certificates and certificate rotation out of the box, however, using Kubernetes for everything may not be a viable option for many enterprises. Therefore, manual or semi-manual deployment and management of internal keys/certificates remains wide-spread.

PKI and encryption implementation by itself does not guarantee security. It requires proper management and control of all the keys and certificates. A compromised key can open doors to many valuable back-end systems. On the other hand, a compromised server with a compromised certificate can collect valuable data from clients and send it to perpetrators.
Case in point – in the Equifax breach, perpetrators were able to obtain access to PII data over secure channels on the internal network.

Here we’ve attempted to come up with a set of best practices that must be followed in order to properly secure internal systems and their components using PKI/TLS.
